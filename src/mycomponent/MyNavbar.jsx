import React from "react";
import { NavLink } from "react-router-dom";

const MyNavbar = () => {
  return (
    <div>
      <NavLink
        to="/teachers/create"
        style={{ marginRight: "30px", textDecoration: "none" }}
      >
        Create Teachers
      </NavLink>
      <NavLink
        to="/teachers"
        style={{ marginRight: "30px", textDecoration: "none" }}
      >
        Teacher
      </NavLink>
    </div>
  );
};

export default MyNavbar;
