import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import CreateTeacher from "./teacher/CreateTeacher";
import ReadAllTeacher from "./teacher/ReadAllTeacher";
import UpdateTeacher from "./teacher/UpdateTeacher";
import ReadSpecificTeacher from "./teacher/ReadSpecificTeacher";

const MyRoutes = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route index element={<div>this is landing page</div>}></Route>
          <Route
            path="teachers"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllTeacher></ReadAllTeacher>}></Route>
            <Route
              path="create"
              element={<CreateTeacher></CreateTeacher>}
            ></Route>
            <Route
              path=":id"
              element={<ReadSpecificTeacher></ReadSpecificTeacher>}
            ></Route>
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateTeacher></UpdateTeacher>}
              ></Route>
            </Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
};

export default MyRoutes;

/* 

Create Teachers  ("localhost:3000/teachers/create") 
Teachers   ("localhost:3000/teachers") 
localhost:3000/teachers/1234124   => read specific Teacher
localhost:3000/teachers/update/1234234   => update teacher










*/

/* 


Navlink
create
read all


Route
create
read all
read specific
update specific


*/
