import axios from "axios";
import { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ReactDropZone from "../../component/ReactDropZone";

// name
// address
// email
//description

const CreateTeacher = () => {
  let [name, setName] = useState("");
  let [address, setAddress] = useState("");

  let [link, setLink] = useState("");

  let handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name: name,
      address: address,
      profileImage: link,
    };

    // console.log("form is submitted", data);
    //hit api

    try {
      let result = await axios({
        url: `http://localhost:8000/teachers`,
        method: "post",
        data: data,
      });

      setName("");
      setAddress("");
      setLink("");

      console.log(result);

      // show toast message

      toast(result.data.message);
    } catch (error) {
      // console.log(error);
      toast(error.response.data.message);
    }
  };

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Name </label>
          <input
            id="name"
            type="text"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="address">Address </label>
          <input
            id="address"
            type="text"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          ></input>
        </div>

        <ReactDropZone setLink={setLink} link={link}></ReactDropZone>

        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default CreateTeacher;

/*  
c====
read all======
read specific
u
d
*/
