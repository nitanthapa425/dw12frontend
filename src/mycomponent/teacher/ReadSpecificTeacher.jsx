import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const ReadSpecificTeacher = () => {
  let [teacher, setTeacher] = useState({});
  let params = useParams();

  /* 
  hit api
  get data
  set data
  
  */

  const getTeacher = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/teachers/${params.id}`,
        method: "get",
      });

    //   console.log(result);
    setTeacher(result.data.result)
    } catch (error) {}
  };

  useEffect(() => {
    getTeacher();
  }, []);

  return (
    <>
      <p>Name is {teacher.name}</p>
      <p>Address is {teacher.address}</p>
    </>
  );
};

export default ReadSpecificTeacher;

/* 
url: http://localhost:8000/teachers/1234214
method:"get"


*/
