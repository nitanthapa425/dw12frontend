import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import showAlert from "../../utils/showAlert";

const ReadAllTeacher = () => {
  let [teachers, setTeachers] = useState([]);

  let navigate = useNavigate();

  /* 
  hit api 
  api gives data
  set data to teachers
  
  */

  // useEffect

  let getTeachers = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/teachers",
        method: "get",
      });

      console.log(result);

      setTeachers(result.data.result); //{}
    } catch (error) {
      console.log(error.message);
    }
  };

  // getTeachers();

  useEffect(() => {
    getTeachers();
  }, []);

  return (
    <>
      <ToastContainer></ToastContainer>
      {teachers.map((item, i) => {
        return (
          <div
            key={i}
            style={{ border: "solid red 3px", marginBottom: "20px" }}
          >
            <p> Name is {item.name}</p>

            <img alt="profileImage" src={item.profileImage}></img>

            <button
              onClick={() => {
                navigate(`/teachers/${item._id}`);
              }}
            >
              View
            </button>
            <button
              onClick={() => {
                navigate(`/teachers/update/${item._id}`);
              }}
            >
              Edit
            </button>
            {/* <button
              onClick={async () => {
                //hit api to delete

                let result = await axios({
                  url: `http://localhost:8000/teachers/${item._id}`,
                  method: "delete",
                });

                getTeachers();

                toast(result.data.message);
              }}
            >
              Delete
            </button> */}

            <button
              onClick={() => {
                showAlert(
                  getTeachers,
                  `http://localhost:8000/teachers/${item._id}`
                );
              }}
            >
              Delete
            </button>
          </div>
        );
      })}
    </>
  );
};

export default ReadAllTeacher;

/* 

page
hit api => data => teacher a,b,c
a, (delete)  
b,
c




database
b
c




*/
