import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

// name
// address
// email
//description

const UpdateTeacher = () => {
  let [name, setName] = useState("");
  let [address, setAddress] = useState("");

  let params = useParams();
  let navigate = useNavigate();

  const getTeacher = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/teachers/${params.id}`,
        method: "get",
      });

      //   console.log(result.data.result);
      setName(result.data.result.name);
      setAddress(result.data.result.address);
      //   setTeacher(result.data.result);
    } catch (error) {}
  };

  useEffect(() => {
    getTeacher();
  }, []);

  let handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name: name,
      address: address,
    };

    // console.log("form is submitted", data);
    //hit api

    try {
      let result = await axios({
        url: `http://localhost:8000/teachers/${params.id}`,
        method: "patch",
        data: data,
      });

      navigate("/teachers");

      //   setName("");
      //   setAddress("");

      //   console.log(result);

      //   // show toast message

      //   toast(result.data.message);
    } catch (error) {
      // console.log(error);
      toast(error.response.data.message);
    }
  };

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Name </label>
          <input
            id="name"
            type="text"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="address">Address </label>
          <input
            id="address"
            type="text"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          ></input>
        </div>

        <button type="submit">Update</button>
      </form>
    </div>
  );
};

export default UpdateTeacher;

/*  
c====
read all======
read specific
u
d
*/
