import React, { useState } from "react";
import Info from "./component/Info";
import EffectOfData from "./component/EffectOfData";
import TernaryOperator from "./component/TernaryOperator";
import ButtonClick from "./component/ButtonClick";
import LearnUseState from "./component/learnUseState/LearnUseState";
import JsPractice1 from "./component/jsPractice/JsPractice1";
import LeanUseState2 from "./component/learnUseState/LeanUseState2";
import HideAndShowImage from "./component/learnUseState/HideAndShowImage";
import HideAndShowImage2 from "./component/learnUseState/HideAndShowImage2";
import OnClickPassValue from "./component/learnUseState/OnClickPassValue";
import Renderng1 from "./component/learnUseState/Rendering/Renderng1";
import GrandParent from "./component/learnUseState/Rendering/GrandParent";
import InfiniteRendering from "./component/learnUseState/InfiniteRendering";
import Form1 from "./component/FormHandling/Form1";
import Form2 from "./component/FormHandling/Form2";
import LernJsonstrpars from "./component/LernJsonstrpars";
import PrimitiveNonPrimitive from "./component/learnUseState/PrimitiveNonPrimitive";
import LearnUseEffect1 from "./component/learnUseEffect/LearnUseEffect1";
import MultipleUseEffect2 from "./component/learnUseEffect/MultipleUseEffect2";
import UseEffectWithAndWithoutDependency from "./component/learnUseEffect/UseEffectWithAndWithoutDependency";
import UseEffectDependency from "./component/learnUseEffect/UseEffectDependency";
import UseEffectWithStringify from "./component/learnUseEffect/UseEffectWithStringify";
import LearnCleanUpFunction from "./component/learnUseEffect/LearnCleanUpFunction";
import LearnUseRef1 from "./component/learnUseRef/LearnUseRef1";
import Parent from "./component/sendData/Parent";
import LearnUseRedcuer1 from "./component/LearnUseReducer/LearnUseRedcuer1";
import LearnUseReducer2 from "./component/LearnUseReducer/LearnUseReducer2";
import LearnUseReducer3 from "./component/LearnUseReducer/LearnUseReducer3";

const App = () => {
  let [show, setShow] = useState(true);

  return (
    <div>
      {/* <div style={{ color: "red" }}>Hello this is div</div> */}
      {/* <Info name="nitan" age={30} address="gagalphedi"></Info> */}
      {/* <EffectOfData></EffectOfData> */}
      {/* <TernaryOperator></TernaryOperator> */}
      {/* <ButtonClick></ButtonClick> */}
      {/* <LearnUseState></LearnUseState> */}
      {/* <JsPractice1></JsPractice1> */}
      {/* <LeanUseState2></LeanUseState2> */}
      {/* <HideAndShowImage></HideAndShowImage> */}
      {/* <HideAndShowImage2></HideAndShowImage2> */}
      {/* <OnClickPassValue></OnClickPassValue> */}
      {/* <Renderng1></Renderng1> */}
      {/* <GrandParent></GrandParent> */}
      {/* <InfiniteRendering></InfiniteRendering> */}
      {/* <Form1></Form1> */}
      {/* <Form2></Form2> */}
      {/* <LernJsonstrpars></LernJsonstrpars> */}
      {/* <PrimitiveNonPrimitive></PrimitiveNonPrimitive> */}
      {/* <LearnUseEffect1></LearnUseEffect1> */}
      {/* <MultipleUseEffect2></MultipleUseEffect2> */}
      {/* <UseEffectWithAndWithoutDependency></UseEffectWithAndWithoutDependency> */}
      {/* <UseEffectDependency></UseEffectDependency> */}
      {/* <UseEffectWithStringify></UseEffectWithStringify> */}

      {/* {show ? <LearnCleanUpFunction></LearnCleanUpFunction> : null}
      <br></br>
      <br></br>
      <br></br>
      <br></br>

      <button
        onClick={() => {
          setShow(true);
        }}
      >
        Show Component
      </button>
      <button
        onClick={() => {
          setShow(false);
        }}
      >
        Hide Component
      </button> */}
      {/* <LearnUseRef1></LearnUseRef1> */}

      {/* <Parent></Parent> */}

      {/* <LearnUseRedcuer1></LearnUseRedcuer1> */}
      {/* <LearnUseReducer2></LearnUseReducer2> */}
      <LearnUseReducer3></LearnUseReducer3>
    </div>
  );
};

export default App;
