import React from "react";
import MyNavbar from "./mycomponent/MyNavbar";
import MyRoutes from "./mycomponent/MyRoutes";

const MyApp = () => {
  return (
    <div>
      <MyNavbar></MyNavbar>
      <MyRoutes></MyRoutes>
    </div>
  );
};

export default MyApp;
