import React, { useState } from "react";

const HideAndShowImage = () => {
  let [show, setShow] = useState(true);

  let handleShow = () => {
    setShow(true);
  };

  let handleHide = () => {
    setShow(false);
  };

  return (
    <div>
      {show ? <img src="./favicon.ico"></img> : null}

      <br></br>
      {/* <button
        onClick={() => {
          setShow(true);
        }}
      >
        Show
      </button> */}

      {/* or */}

      <button onClick={handleShow}>Show</button>

      <br></br>

      {/* <button
        onClick={() => {
          setShow(false);
        }}
      >
        Hide
      </button> */}

      {/* or */}

      <button onClick={handleHide}></button>
    </div>
  );
};

export default HideAndShowImage;
