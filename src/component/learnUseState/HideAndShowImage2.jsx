import React, { useState } from "react";

const HideAndShowImage2 = () => {
  let [show, setShow] = useState(true);
  //true//false//true

  let handleToggle = () => {
    setShow(!show);
  };

  return (
    <>
      {show ? <img src="./favicon.ico"></img> : null}
      <br></br>
      {/* <button
        onClick={() => {
          setShow(!show);
        }}
      >
        Show and Hide
      </button> */}
      {/* or */}
      <button onClick={handleToggle}>Show and Hide</button>
    </>
  );
};

export default HideAndShowImage2;
