import React, { useState } from "react";

const InfiniteRendering = () => {
  let [count1, setCount1] = useState(0); //1//2//3

  setCount1(count1 + 1);
  //line  no 6 result infinite rendering

  //  to prevent infinite rendering
  // always place setCount1 on event (button click ,....) or inside useEffect

  return (
    <div>
      count1 is {count1}
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
    </div>
  );
};

export default InfiniteRendering;
