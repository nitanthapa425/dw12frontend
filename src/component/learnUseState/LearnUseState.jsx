import React, { useState } from "react";

const LearnUseState = () => {
  // let age =30

  /* 
  define
  call
  
  
  */

  let [age, setAge] = useState(30);

  return (
    <div>
      {age}
      <br></br>
      <button
        onClick={() => {
          setAge(31);
        }}
      >
        click me
      </button>
    </div>
  );
};

export default LearnUseState;
