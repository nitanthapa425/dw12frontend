import React from "react";

const OnClickPassValue = () => {
  let handleClick1 = () => {
    console.log("button is clicked");
  };

  let handleClick2 = (a, b) => {
    return () => {
      console.log("button is clicked");
    };
  };
  return (
    <div>
      <button onClick={handleClick1}>click me1 </button>

      <button onClick={handleClick2(1, 2)}>click me2</button>
      {/* the above technique is used if we need to pass value */}
    </div>
  );
};

export default OnClickPassValue;
