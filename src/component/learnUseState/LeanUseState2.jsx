import React, { useState } from "react";

const LeanUseState2 = () => {
  let [count1, setCount1] = useState(0);
  return (
    <>
      <p> ****** count1 is {count1}</p>
      <button
        onClick={() => {
          
         
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 - 1);
        }}
      >
        Decrement count 1
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount1(0);
        }}
      >
        Reset count 1
      </button>
    </>
  );
};

export default LeanUseState2;
