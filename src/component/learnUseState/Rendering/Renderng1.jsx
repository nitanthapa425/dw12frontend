import React, { useEffect, useState } from "react";

const Renderng1 = () => {
  //why useState

  console.log("hello");

  let [count1, setCount1] = useState(0); //1//2//3//4

  let [count2, setCount2] = useState(100); //101//102//103//104

  return (
    <div>
      <p>count1 is {count1}</p>
      <p> count2 is {count2}</p>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
      <button
        onClick={() => {
          setCount2(count2 + 1);
        }}
      >
        Increment count2
      </button>
    </div>
  );
};

export default Renderng1;

/* 
1) when state variable changes
a component will render (execute)


2) component will only render if state variable changes , if it does not component will not render


3) when a component is render by count1 state variable ( the count1 variable will be updated)   (other state variable  such as count2 ,... will store  previous value)



*/
