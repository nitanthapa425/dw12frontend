import React from "react";
import Child from "./Child";

const Parent = ({ count1 }) => {
  console.log("parent component gets render");

  return (
    <div>
      <Child count1={count1}></Child>
    </div>
  );
};

export default Parent;

/* 

prop drilling



*/
