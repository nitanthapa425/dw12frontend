import React, { useState } from "react";
import Parent from "./Parent";

const GrandParent = () => {
  console.log("grandparent component gets render");
  let [count1, setCount1] = useState(0);
  return (
    <div>
      <p>count1 is {count1}</p>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
      <Parent count1={count1}></Parent>
    </div>
  );
};

export default GrandParent;

/* 
grandParent   (count1)
parent
child
AA
BB
CC
DD




stor
count1


*/
