import React from "react";

const Child = ({ count1 }) => {
  console.log("child component gets render");
  return <div>Hello i nested count1 is {count1}</div>;
};

export default Child;
