import React, { useState } from "react";

const PrimitiveNonPrimitive = () => {
  // 1,true,"nitan"...  => === => value
  // [], {}   => ===  address


  let [count, setCount] = useState(1);

  let [list, setList] = useState([1, 2, 3]);
  console.log("hello");
  return (
    <div>
      <button
        onClick={() => {
          setCount(1);
        }}
      >
        Change count
      </button>

      <button
        onClick={() => {
          setList([1, 2, 3]);
        }}
      >
        change list
      </button>
    </div>
  );
};

export default PrimitiveNonPrimitive;

/* 
not render case
in case of primitive 
    if value are same

in case of non primitive
  if address are same


*/
