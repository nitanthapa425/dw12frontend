import axios from "axios";
import React from "react";
import { useCallback } from "react";
import { useDropzone } from "react-dropzone";

const ReactDropZone = ({ setLink, link }) => {
  const onDrop = useCallback(async (acceptedFiles) => {
    // Do something with the files
    // hit api
    // api gives links
    //set links

    let formData = new FormData();
    formData.append("file1", acceptedFiles[0]);

    try {
      let result = await axios({
        url: "http://localhost:8000/files/single",
        method: "post",
        data: formData,
      });
      //tost file upload successfully

      setLink(result.data.result);
    } catch (error) {
      //tost ....
      console.log(error.message);
    }
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <div>
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        {isDragActive ? (
          <p>Drop the files here ...</p>
        ) : (
          <p>Drag 'n' drop some files here, or click to select files</p>
        )}
        {link ? <img alt="profile-image" src={link}></img> : null}
      </div>
    </div>
  );
};

export default ReactDropZone;
