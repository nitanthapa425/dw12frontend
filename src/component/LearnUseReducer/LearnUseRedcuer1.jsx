import React, { useReducer } from "react";

//another way to make variable
//useState//primitive  (number, string, boolean,null, undefined)
//useReducer// (arr , obj => useReucer)

//define variable using useReducer
//call variable
//change variable of useReducer
//dispatch is used to change useReducer variable

const LearnUseRedcuer1 = () => {
  //   let [name, setName] = useState("nitan");

  //             thapnitaram, ram
  let reducer = (state, action) => {
    return `thapa${state}${action}`; //thapthapanitaramram
  };

  let [name, dispatch] = useReducer(reducer, "nitan"); //"nitan""//thapanitanram
  return (
    <div>
      {name}

      <button
        onClick={() => {
          dispatch("ram");
        }}
      >
        Change Name
      </button>
    </div>
  );
};

export default LearnUseRedcuer1;
