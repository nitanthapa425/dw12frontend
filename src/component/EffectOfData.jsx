import React from "react";

const EffectOfData = () => {
  let name = "nitan";
  let age = 29;
  let isMarried = false; //boolean are not shown in browser
  let tags = [<div>Hello1</div>, <div>Hello2</div>, <div>Hello3</div>];
  let info = { name: "nitan", age: 30 };
  let a = null; //null is not shown in browser
  let b = undefined; //null is not shown in browser
  return (
    <div style={{ backgroundColor: "red" }}>
      <p>{name}</p>
      <p>{age}</p>
      <p>{isMarried}</p>
      <p>{tags}</p>
      {/* <p>{info}</p> */}
      <p>{info.name}</p>
      <p>{info.age}</p>
      {a}
      {b}
    </div>
  );
};

export default EffectOfData;

/* 

boolean are not displayed in browser
when array is placed in the browser=> it place element one by one (note [] and , are not written in browser)
we can not place object in browser (Error: Object is not valid as react child)
*/