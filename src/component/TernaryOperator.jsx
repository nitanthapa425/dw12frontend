import React from "react";

// age<18 print <div> He is Underage</div>
// age>=18 && age<60   print <div>He is Adult</div>
// Above 60 print <div>He isOld</div>

const TernaryOperator = () => {
  let age = "aslkdfj";
  return (
    <div>
      {age < 18 ? (
        <div>he/she is Underage</div>
      ) : age > 18 && age <= 60 ? (
        <div>Adult</div>
      ) : age > 60 ? (
        <div>He is old</div>
      ) : null}
    </div>
  );
};

export default TernaryOperator;
