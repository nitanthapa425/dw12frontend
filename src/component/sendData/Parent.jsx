import React from "react";
import Child from "./Child";

const Parent = () => {
  let fun1 = (data) => {
    console.log(data);
  };

  return (
    <div>
      <Child fun1={fun1}></Child>
    </div>
  );
};

export default Parent;
//sending data from child to parent
// make fun in parent
//send func to child
// send data to than fun
