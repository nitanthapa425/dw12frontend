import React from "react";

const Child = ({ fun1 }) => {
  let data = "nitan";
  return (
    <div>
      Child
      <button
        onClick={() => {
          fun1(data);
        }}
      >
        Send Data
      </button>
    </div>
  );
};

export default Child;
