import React, { useRef } from "react";

const LearnUseRef1 = () => {
  let ref1 = useRef();
  let ref2 = useRef();
  let ref3 = useRef();
  const handleClick = () => {
    ref1.current.style.color = "red";
    ref2.current.style.color = "green";
  };

  const manipulateInput = () => {
    ref3.current.focus();
  };
  return (
    <div>
      <p ref={ref1}>This is paragraph 1</p>
      <p ref={ref2}>This is paragraph 2</p>
      <button onClick={handleClick}>Change color</button>

      <br></br>
      <div onClick={manipulateInput}>Name</div>
      <input id="name" ref={ref3}></input>
    </div>
  );
};

export default LearnUseRef1;

/* 
ref is use 
if you want to control one element by other element
process
define useRef
attach ref (in those element which wants to be manipulated)
ref.current

*/
