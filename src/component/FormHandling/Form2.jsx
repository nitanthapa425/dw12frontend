import React, { useState } from "react";

// fullName
// address
// email
//description

const Form2 = () => {
  let [fullName, setFullName] = useState("");
  let [address, setAddress] = useState("");
  let [email, setEmail] = useState("");
  let [description, setDescription] = useState("");
  let [isMarried, setIsMarried] = useState(false);
  let [country, setCountry] = useState("1");
  let [gender, setGender] = useState("male");

  let handleSubmit = (e) => {
    e.preventDefault();
    let data = {
      fullName: fullName,
      address: address,
      email: email,
      description: description,
      isMarried: isMarried,
      country: country,
      gender: gender,
    };

    console.log("form is submitted", data);
  };

  let countries = [
    { label: "Nepal", _id: "1" },
    { label: "India", _id: "2" },
    { label: "China", _id: "3" },
    { label: "Pakistan", _id: "4" },
  ];

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="fullName">Full Name </label>
          <input
            id="fullName"
            type="text"
            value={fullName}
            onChange={(e) => {
              setFullName(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="address">Address </label>
          <input
            id="address"
            type="text"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="email">Email </label>
          <input
            id="email"
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="description">Description </label>
          <textarea
            id="description"
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
          ></textarea>
        </div>

        <div>
          <label htmlFor="isMarried">Is Married? </label>
          <input
            type="checkbox"
            id="isMarried"
            checked={isMarried}
            onChange={(e) => {
              setIsMarried(e.target.checked);
            }}
          ></input>
        </div>

        <div>
          <label>Country</label>
          <select
            value={country}
            onChange={(e) => {
              setCountry(e.target.value);
            }}
          >
            {/* <option value="1">Nepal</option>
            <option value="2">India</option>
            <option value="3">China</option>
            <option value="4">Pakistan</option> */}
            {countries.map((item, i) => {
              return (
                <option key={i} value={item._id}>
                  {item.label}
                </option>
              );
            })}
          </select>
        </div>

        {/* radio button */}

        <div>
          <label htmlFor="male">Gender </label>

          {/* <label htmlFor="male">Male</label>
          <input
            type="radio"
            id="male"
            value="male"
            checked={gender === "male"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input>
          <label htmlFor="female">Female</label>
          <input
            type="radio"
            id="female"
            value="female"
            checked={gender === "female"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input>
          <label htmlFor="other">Other</label>
          <input
            type="radio"
            id="other"
            value="other"
            checked={gender === "other"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input> */}

          {genders.map((item, i) => {
            return (
              <span key={i}>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  id={item.value}
                  value={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </span>
            );
          })}
        </div>

        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default Form2;

/*  */
