import React from "react";

const JsPractice1 = () => {
  let products = [
    {
      id: 1,
      title: "MacBook Pro",
      category: "Laptops",
      price: 100000.0,
      quantity: 2,
      description: "A high-performance laptop.",
      manufactureDate: "2023-05-15T08:30:00",
      isAvailable: true,
    },
    {
      id: 2,
      title: "Nike",
      category: "Running Shoes",
      price: 5000,
      quantity: 3,
      description: "Running shoes designed for speed and comfort.",
      manufactureDate: "2023-02-20T14:45:00",
      isAvailable: true,
    },
    {
      id: 3,
      title: "Python",
      category: "Books",
      price: 500,
      quantity: 1,
      description: "A language for AI",
      manufactureDate: "1925-04-10T10:10:00",
      isAvailable: false,
    },
    {
      id: 4,
      title: "Javascript",
      category: "Books",
      price: 700,
      quantity: 5,
      description: "A language for Browser",
      manufactureDate: "1995-12-04T12:00:00",
      isAvailable: false,
    },
    {
      id: 5,
      title: "Dell XPS",
      category: "Laptops",
      price: 120000.0,
      quantity: 2,
      description: "An ultra-slim laptop with powerful performance.",
      manufactureDate: "2023-04-25T09:15:00",
      isAvailable: true,
    },
  ];

  //react fragment

  let task1 = () => {
    let output = products.map((item, i) => {
      return <div key={i}>{item.title}</div>;
    });

    return output;
  };
  let task2 = () => {
    let output = products.map((item, i) => {
      return (
        <div key={i}>
          {item.title} cost NRs. {item.price} and its category is
          {item.category}
        </div>
      );
    });

    return output;
  };

  let task3 = () => {
    //filter
    //map

    let output = products
      .filter((item, i) => {
        if (item.price > 2000) {
          return true;
        }
      })
      .map((item, i) => {
        return (
          <div key={i}>
            {item.title} costs NRs. {item.price} and its category is{" "}
            {item.category}
          </div>
        );
      });

    return output;
  };

  let task4 = () => {
    let totalPrice = products.reduce((pv, cv) => {
      return pv + cv.price;
    }, 0);

    return totalPrice;
  };

  let task5 = () => {
    let ar1 = products.map((value, i) => {
      return value.category;
    });

    let uniqueAr1 = [...new Set(ar1)];

    let output = uniqueAr1.map((value, i) => {
      return <div key={i}>{value}</div>;
    });

    return output;
  };

  return (
    <>
      <h1>The product in our shop is</h1>
      {/* {task1()} */}
      {/* {task2()} */}
      {/* {task3()} */}

      {/* <p>the total price of all product is NRs.{task4()}</p> */}
      {task5()}
    </>
  );
};

export default JsPractice1;

// 500 lakh $   = > 50
