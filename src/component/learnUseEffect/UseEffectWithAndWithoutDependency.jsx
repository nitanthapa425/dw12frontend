import React, { useEffect, useState } from "react";

const UseEffectWithAndWithoutDependency = () => {
  let [count1, setCount1] = useState(0);
  let [count2, setCount2] = useState(100);

  console.log("component is render");

  // useEffect(() => {
  //   console.log("i am useEffect 1");
  // }); //without dependency
  // it will execute in every render

  // useEffect(() => {
  //   console.log("i am useEffect 2");
  // }, []); // with empty dependency
  // // it will run in first render only

  // useEffect(() => {
  //   console.log("i am useEffect 3");
  // }, [count1]); // with dependency (count1)
  // it will expecute in first rendern
  // but from second render it will execute if count1 value changes

  useEffect(() => {
    console.log("i am useEffect 3");
  }, [count1, count2]); // with dependency (count1, count2)
  // runs at first render
  // from second render the execution of useEffect depend on count , count2
  return (
    <div>
      <p>count1 is {count1}</p>
      <p> count2 is {count2}</p>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
      <button
        onClick={() => {
          setCount2(count2 + 1);
        }}
      >
        increment count2
      </button>
    </div>
  );
};

export default UseEffectWithAndWithoutDependency;
