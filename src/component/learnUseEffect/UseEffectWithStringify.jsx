import React, { useEffect, useState } from "react";

const UseEffectWithStringify = () => {
  let [list, setList] = useState([1, 2]); //non primitive
  let [name, setName] = useState("nitan"); //primitive

  useEffect(() => {
    console.log(list);
    console.log(name);
  }, [JSON.stringify(list), name]);
  return <div>UseEffectWithStringify</div>;
};

export default UseEffectWithStringify;

/* 
if dependency is non primitive 
always wrap it by JSON.stringify()
*/
