import React, { useEffect } from "react";

const LearnUseEffect1 = () => {
  console.log("1");
  useEffect(() => {
    console.log("i am useEffect");
  }, []);
  console.log("2");

  return <div>LearnUseEffect1</div>;
};

export default LearnUseEffect1;

/* 
useEffect is asynchronous function
useEffect will run just after the dom is printed on browser

output will be 1
1
2
i am useEffect
*/
