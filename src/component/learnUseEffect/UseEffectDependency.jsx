import React, { useEffect, useState } from "react";

const UseEffectDependency = () => {
  let a = 10;
  let b = 15;

  let [name, setName] = useState("nitan");
  let [age, setAge] = useState(30);

  useEffect(() => {
    console.log(a);
    console.log(age);
  }, [a, age]);
  // any variable that is used inside useEffect => try to use that variable inside dependency

  return <div>UseEffectDependency</div>;
};

export default UseEffectDependency;
