import React, { useEffect, useState } from "react";

const LearnCleanUpFunction = () => {
  let [count, setCount] = useState(0);

  /* 
  1st render
    use Effect runs
    but cleanup func does not execute
  2n render (count chage)
    use Effect runs
    clean up runs
  same for all render

  */
  //   useEffect(() => {
  //     console.log("i am useEffect");

  //     return () => {
  //       console.log("i am clean up");
  //     };
  //   }, []);

  useEffect(() => {
    let interval1 = setInterval(() => {
      console.log("i will execute after each 2s");
    }, 2000);

    return () => {
      clearInterval(interval1);
    };
  }, []);

  return (
    <div style={{ border: "solid red 3px" }}>
      count is {count}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnCleanUpFunction;

/* 
clean up function are the function return by useEffect
property (execution)
    it does not execute in first render
    from second render clean up fun will execute
    during execution of useEffect first clean up execute the other part of useEffect code gets execute

    mount (first render)
    all code execute except clean up function

    unmount
    not execute but clean up function gets executed
why it is named as clean up




component mount
component unmount


Explain usEffect
Explain clean up function 
Life cycle of component

component did mount  (first render) => useEffect
component did update (from 2 render)
component din unmount ( component remove)





*/
