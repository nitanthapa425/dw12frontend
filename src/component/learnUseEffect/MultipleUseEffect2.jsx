import React, { useEffect } from "react";

const MultipleUseEffect2 = () => {
  console.log(1);
  useEffect(() => {
    console.log("i am first useEffect");
  }, []);
  useEffect(() => {
    console.log("i am second useEffect");
  }, []);
  console.log(2);

  return <div>MultipleUseEffect2</div>;
};

export default MultipleUseEffect2;

/* 
we can have multiple useEffect
 */
