import React from "react";

const LernJsonstrpars = () => {
  let info = { name: "nitan", age: 30 };

  let infoString = JSON.stringify(info); //'{ name: "nitan", age: 30 };'

  localStorage.setItem("info", infoString);

  let localStorageData = localStorage.getItem("info"); //'{name:"nitan", age:30"}'
  let parsLocalStorageData = JSON.parse(localStorageData);
  console.log(parsLocalStorageData); //{name:"nitan", age:30"}

  //   let infoString = JSON.stringify(info); //'{ name: "nitan", age: 30 };'
  //   let normalInfo = JSON.parse(infoString); // {name:"nitan",age:30}
  //   console.log(infoString);
  //   console.log(normalInfo);
  return <div>LernJsonstrpars</div>;
};

export default LernJsonstrpars;

// obj or array => stringify => localStorage
//localStorage => parse => obj or array
