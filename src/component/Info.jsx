//old way
// import React from "react";

// const Info = (props) => {
//   //props = { name:"nitan", age:30, address:"gagalphedi"}
//   console.log(props);
//   return (
//     <div>
//       <p> Name is {props.name}</p>
//       <p>Age is {props.age}</p>
//       <p>Address is {props.address}</p>
//     </div>
//   );
// };

// export default Info;

//new way
import React from "react";

const Info = ({ name, age, address }) => {
  return (
    <div>
      <p>Nam is {name}</p>
      <p>age is {age}</p>
      <p>Address is {address}</p>
    </div>
  );
};

export default Info;
